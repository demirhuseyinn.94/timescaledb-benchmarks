# Test Environments


```bash

Recommendations based on 31.28 GB of available memory and 12 CPUs for PostgreSQL 12

Memory settings recommendations
success: memory settings are already tuned

Parallelism settings recommendations
success: parallelism settings are already tuned

WAL settings recommendations
success: WAL settings are already tuned

Miscellaneous settings recommendations
success: miscellaneous settings are already tuned
Saving changes to: /var/lib/postgresql/data/postgresql.conf


```

1. Traditional postgresql table model

```sql

DROP TABLE IF EXISTS "readings_traditional";
CREATE TABLE "readings_traditional"(
    time  TIMESTAMP WITH TIME ZONE NOT NULL,
    device_id  TEXT,
    battery_level  DOUBLE PRECISION,
    battery_status  TEXT,
    battery_temperature  DOUBLE PRECISION,
    bssid  TEXT,
    cpu_avg_1min  DOUBLE PRECISION,
    cpu_avg_5min  DOUBLE PRECISION,
    cpu_avg_15min  DOUBLE PRECISION,
    mem_free  DOUBLE PRECISION,
    mem_used  DOUBLE PRECISION,
    rssi  DOUBLE PRECISION,
    ssid  TEXT
);
```


2. TimescaleDB Hypertable Model

```sql
DROP TABLE IF EXISTS "readings";
CREATE TABLE "readings"(
    time  TIMESTAMP WITH TIME ZONE NOT NULL,
    device_id  TEXT,
    battery_level  DOUBLE PRECISION,
    battery_status  TEXT,
    battery_temperature  DOUBLE PRECISION,
    bssid  TEXT,
    cpu_avg_1min  DOUBLE PRECISION,
    cpu_avg_5min  DOUBLE PRECISION,
    cpu_avg_15min  DOUBLE PRECISION,
    mem_free  DOUBLE PRECISION,
    mem_used  DOUBLE PRECISION,
    rssi  DOUBLE PRECISION,
    ssid  TEXT
);
CREATE INDEX ON "readings"(time DESC);
CREATE INDEX ON "readings"(device_id, time DESC);
-- 86400000000 is in usecs and is equal to 1 day
SELECT create_hypertable('readings', 'time', chunk_time_interval => 86400000000);

```

# Steps to reproduce

1. Start timescaledb docker container

```bash

docker run -d --name timescaledb -p 5432:5432 -e POSTGRES_PASSWORD=password timescale/timescaledb:latest-pg12

```
2. Use COPY command to insert the data

# Data Source

https://docs.timescale.com/timescaledb/latest/tutorials/sample-datasets/#in-depth-devices


# Results

| #                            | Total Rows(integer) | Op Type(integer) | Duration(seconds) |
|------------------------------|---------------------|------------------|-------------------|
| Traditional PostgreSQL Table | 30.000.000          | INSERT           | 93                |
| Timescale Hypertable         | 30.000.000          | INSERT           | 253               |
